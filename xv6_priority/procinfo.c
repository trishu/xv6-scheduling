#include "types.h"
#include "stat.h"
#include "user.h"
#include "processInfo.h"
int main(int argc, char *argv[])
{
    struct processInfo info;
    int pid;
    printf(1, "PID\tParent\ttickets\tpriority\tSIZE\tName\tState\tNumber of Context Switch\n");
    for(int i=1; i<=getmaxpid(); i++)
    {
        pid = i;
        if(getprocinfo(pid, &info) == 0)
	  printf(1, "%d\t%d\t%d\t%d\t%d\t%s\t%d\t%d\n", info.pid, info.parent, info.tickets, info.priority, info.psize, info.pname,   info.state, info.numberContexSwitches);
    }
    exit();
}
