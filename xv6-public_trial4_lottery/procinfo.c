#include "types.h"
#include "stat.h"
#include "user.h"
#include "processInfo.h"
int main(int argc, char *argv[])
{
    struct processInfo info;
    int pid;
    printf(1, "PID\tParent\ttickets\tTotal\tSIZE\tName\tNumber of Context Switch\n");
    for(int i=1; i<=getmaxpid(); i++)
    {
        pid = i;
        if(getprocinfo(pid, &info) == 0)
	  printf(1, "%d\t%d\t%d\t%d\t%d\t%s\t%d\n", info.pid, info.parent, info.tickets,info.total, info.psize, info.pname, info.numberContexSwitches);
    }
    exit();
}
