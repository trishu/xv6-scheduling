struct processInfo
{
    int pid;
    int parent;
    int tickets;
    int total;
    int psize;
    char pname[16];
    int numberContexSwitches;
};
