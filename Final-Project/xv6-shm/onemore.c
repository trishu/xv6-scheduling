#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	int pid;
	int key = 12;
	pid = fork();
	if(pid == 0){
			printf(1,"from child\n");
			char c;
			int shmid;
			
			int size = 51;
			char *shm, *s;
		
	
			if ((shmid = shmget(key, size)) < 0) {
			
				printf(1,"shmget failed %d\n", shmid);
				exit();
			}	
			if ((shm = shmat(shmid, "mahi")) == (char *) -1) {
				printf(1,"shmat");
				exit();
			}
				printf(1, "returned addr of shmat is %x\n", shm);
		
				s = shm;


			for (c = 'A'; c <= 'Z'; c++)
				*s++ = c;
			*s = 0;
	
		printf(1,"Child written data is %s\n", shm);

		printf (1,"waiting for parent to write *\n");
		while (*shm != '*')
			sleep(1);
		printf(1, "In child changed data is %s\n", shm);
		printf (1,"wait over\n");
		exit();
	}
	else{
		//int x = wait();
		sleep(10);
		int shmid;
		int  size = 51;
		char *shm;

		printf(1,"from parent \n");
		
		

		if ((shmid = shmget(key, size)) < 0) {
			printf(1,"shmget");
			exit();
		}

		if ((shm = shmat(shmid, "mahi")) == (char *) -1) {
			printf(1,"shmat");
			exit();
		}
		printf(1, "returned addr of shmat is %x\n", shm);


		printf(1,"Parent Reading data from shared memory %s\n", shm);
		printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));

		sleep(5);
		printf(1,"Parent Writing *\n");
		*shm = '*';
	wait();
	}
		printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
		printf(1, "-----------------------------------------------------\n");
		printf(1, "working of shm --- on proc writes data, other proc reads it, changes it and pass it again\n");
	exit();
}
