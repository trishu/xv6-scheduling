#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	int key = 8;
	int size = 51;
	int key1 = 9;
	int size1= 100;
	char c;
	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s = shm;
	for (c = 'A'; c <= 'N'; c++)
		*s++ = c;
	*s = 0;
	
	printf(1, "writeen data is %s\n", shm);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	
	int id1 = shmget(key1, size1);
	
	if(id1 < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm1 = (char*)shmat(id1, "mahi");
	if(shm1 == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s1 = shm1;
	for (c = 'A'; c <= 'K'; c++)
		*s1++ = c;
	*s1 = 0;
	
	printf(1, "writeen data is %s\n", shm1);

	printf(1, "No of ref to key %d are %d\n", key1, shm_ref_cnt(key1));

	int pid = fork();
	if(pid == 0){
		printf(1, "I'm child\n");
		printf(1, "my pid is %d \n", getpid());
		printf(1, "data read from child 1st shm is %s\n", shm);
		printf(1, "data read from child 2nd shm is %s\n", shm1);
		printf(1, "No of ref to key %d are %d\n", key1, shm_ref_cnt(key1));
		printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
		exit();
	}
	else{
		wait();
	}
	printf(1, "----------------------------------------------------------\n");
	printf(1, "Child can inherit all the shm segs of parent, not only one\n");
	exit();
}
