#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	int key = 11;
	int size = 100;
	char c;
	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm);
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s = shm;
	for (c = 'A'; c <= 'Z'; c++)
		*s++ = c;
	*s = 0;
	
	printf(1, "writeen data is %s\n", shm);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	
	int x = shmdt(id, shm);
	if(x == -1){
		printf(1, "shmdt is failed\n");
		exit();
	}
	
	printf(1, "after shmdt, No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	//printf(1, "data should not be read after deattching , trap 14 is generead %s\n", shm);
	
	printf(1, "Trying to attach it again\n");
	char *shm2 = shmat(id, "maa");
	if(shm2 == (char*)-1){
		printf(1, "shmat failed in again attaching");
		exit();
	}
	printf(1, "returned addr of shmat is %x\n", shm2);
	printf(1,"data after again attching %x is %s\n",shm2, shm2);
	printf(1, "after shmat, No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	
	printf(1, "-----------------------------------------------------------\n");
	printf(1, "we can deattch shm seg & then attach it again, to the same virtural addr it was prev.. ref cnt also works nicely\n");

	exit();
}
