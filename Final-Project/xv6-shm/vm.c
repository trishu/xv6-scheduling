#include "param.h"
#include "types.h"
#include "defs.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "elf.h"

extern char data[];  // defined by kernel.ld
pde_t *kpgdir;  // for use in scheduler()


// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
  struct cpu *c;

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpuid()];
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
  lgdt(c->gdt, sizeof(c->gdt));
}

// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
  if(*pde & PTE_P){
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
      return 0;
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
}

// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}

// There is one page table per process, plus one that's used when
// a CPU is not running any process (kpgdir). The kernel uses the
// current process's page table during system calls and interrupts;
// page protection bits prevent user code from using the kernel's
// mappings.
//
// setupkvm() and exec() set up every page table like this:
//
//   0..KERNBASE: user memory (text+data+stack+heap), mapped to
//                phys memory allocated by the kernel
//   KERNBASE..KERNBASE+EXTMEM: mapped to 0..EXTMEM (for I/O space)
//   KERNBASE+EXTMEM..data: mapped to EXTMEM..V2P(data)
//                for the kernel's instructions and r/o data
//   data..KERNBASE+PHYSTOP: mapped to V2P(data)..PHYSTOP,
//                                  rw data + free physical memory
//   0xfe000000..0: mapped direct (devices such as ioapic)
//
// The kernel allocates physical memory for its heap and for user memory
// between V2P(end) and the end of physical memory (PHYSTOP)
// (directly addressable from end..P2V(PHYSTOP)).

// This table defines the kernel's mappings, which are present in
// every process's page table.
static struct kmap {
  void *virt;
  uint phys_start;
  uint phys_end;
  int perm;
} kmap[] = {
 { (void*)KERNBASE, 0,             EXTMEM,    PTE_W}, // I/O space
 { (void*)KERNLINK, V2P(KERNLINK), V2P(data), 0},     // kern text+rodata
 { (void*)data,     V2P(data),     PHYSTOP,   PTE_W}, // kern data+memory
 { (void*)DEVSPACE, DEVSPACE,      0,         PTE_W}, // more devices
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (P2V(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
                (uint)k->phys_start, k->perm) < 0) {
      freevm(pgdir);
      return 0;
    }
  return pgdir;
}

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
  kpgdir = setupkvm();
  switchkvm();
}

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
  lcr3(V2P(kpgdir));   // switch to the kernel page table
}

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
  if(p == 0)
    panic("switchuvm: no process");
  if(p->kstack == 0)
    panic("switchuvm: no kstack");
  if(p->pgdir == 0)
    panic("switchuvm: no pgdir");

  pushcli();
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
                                sizeof(mycpu()->ts)-1, 0);
  mycpu()->gdt[SEG_TSS].s = 0;
  mycpu()->ts.ss0 = SEG_KDATA << 3;
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
  // setting IOPL=0 in eflags *and* iomb beyond the tss segment limit
  // forbids I/O instructions (e.g., inb and outb) from user space
  mycpu()->ts.iomb = (ushort) 0xFFFF;
  ltr(SEG_TSS << 3);
  lcr3(V2P(p->pgdir));  // switch to process's address space
  popcli();
}

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
  char *mem;

  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
  memmove(mem, init, sz);
}

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
      panic("loaduvm: address should exist");
    pa = PTE_ADDR(*pte);
    if(sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, P2V(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
}

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
    mem = kalloc();
    if(mem == 0){
      cprintf("allocuvm out of memory\n");
      deallocuvm(pgdir, newsz, oldsz);
      return 0;
    }
    memset(mem, 0, PGSIZE);
    if(mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
      cprintf("allocuvm out of memory (2)\n");
      deallocuvm(pgdir, newsz, oldsz);
      kfree(mem);
      return 0;
    }
  }
  return newsz;
}

// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  pte_t *pte;
  uint a, pa;
	int i, j;
  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
      pa = PTE_ADDR(*pte);
      if(pa == 0)
        panic("kfree");
      char *v = P2V(pa);
      /*we will check whether the pa(phy addr is used by some other process as well)*/
      /*even though if process exit we will keep its shm-pages*/
      int found = 0;
      for(i = 0; i < MAX_KEYS; i++){
      	int used = page_pa[i].used;
      	if(used == 0)
      		continue;
      	int num_pages = page_pa[i].no_of_used_pages;
      	//cprintf("num of p %d, %d\n",i, num_pages);
      	for(j = 0; j < num_pages; j++){
      		//cprintf("page_pa %x, pa %x\n", page_pa[i].pages[j], v);
      		if(page_pa[i].pages[j] == v){
      			//cprintf("smae dind\n");
      			found = 1;
      			break;
      		}
      	}
      	if(found == 1)
      		break;
      }
      if(found == 0){
      	//cprintf("kfree is done");
     	kfree(v);
     }
     /*else{
     	cprintf("kfree is not done");
     }*/
      *pte = 0;
    }
  }
  return newsz;
}

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
  uint i;
  if(pgdir == 0)
    panic("freevm: no pgdir");
   //struct proc* curproc = myproc();
   //cprintf("kernables %x, pid = %d, curproc->top %x\n", KERNBASE, curproc->pid, curproc->top);
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
    if(pgdir[i] & PTE_P){
    	     char * v = P2V(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
}

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if(pte == 0)
    panic("clearpteu");
  *pte &= ~PTE_U;
}

void
setpteu(pde_t *pgdir, char *uva)
{
  pte_t *pte;
	//cprintf("need to go ahead %x\n", uva);
  pte = walkpgdir(pgdir, uva, 0);
  //cprintf("walkpgdir is done success\n");
  if(pte == 0)
    panic("clearpteu");
  *pte |= PTE_U;
  /*if((*pte & PTE_U) == 0){
  	cprintf("it is not user accessible page\n");
  }
  else{
  	cprintf("it is user accessible page\n");
  }*/

}

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

	//cprintf("curproc is %d, and top is %x\n", myproc()->pid, myproc()->top);
  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
      panic("copyuvm: pte should exist");
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0) {
      kfree(mem);
      goto bad;
    }
  }
  
  struct proc *curproc = myproc();
  for(i = KERNBASE - PGSIZE; i >= curproc->top; i -= PGSIZE){
  	if((pte = walkpgdir(pgdir, (void*)i, 0)) == 0)
  		panic("copyuvm: pte should exist");
  	if(!(*pte & PTE_P))
  		panic("copyuvm: page not present");
  	pa = PTE_ADDR(*pte);
  	flags = PTE_FLAGS(*pte);
  	if(mappages(d, (void*)i, PGSIZE, pa, flags) < 0)
  		goto bad;
  }
 // cprintf("curp")
    return d;

bad:
  freevm(d);
  return 0;
}

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if((*pte & PTE_P) == 0)
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}

// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
}

//PAGEBREAK!
// Blank page.
//PAGEBREAK!
// Blank page.
//PAGEBREAK!
// Blank page.


/**shm functions**/

int
deallocuvmforshm(pde_t *pgdir, uint oldsz, uint newsz)
{
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
      pa = PTE_ADDR(*pte);
      if(pa == 0)
        panic("kfree");
      char *v = P2V(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
}




void
shminit(void){
	int i, j;
	for(i = 0; i < MAX_KEYS; i++){
		keys[i].used = 0;
		keys[i].ref_cnt = 0;
		keys[i].size = 0;

		page_pa[i].used = 0;
		for(j = 0; j < MAX_PAGES; j++){
			page_pa[i].pages[j] = 0;
		}
		page_pa[i].no_of_used_pages = 0;
	}
	cprintf("shm init done\n");
}

//on failure , it return -1 
//on sucess, it return id , which is used by shmat
//all kernel side of things are done in this fun .. ie. nothing regarding process is set here

/*if someone has called shmget, 
	but does not called shmat on it 
	then, what should do ?
	here, key_used = 1 
	but ref_cnt = 0;
*/

int
shmget(int key, int size){
	/*check for both are args, and their corner casses*/
	if(key < 0 || key > 15 || size == 0)
		return -1;
	cprintf("key is %d, size is %d\n",key, size);
	int num_pages = size / PGSIZE;
	int extra = size % PGSIZE;
	if(extra != 0){
		num_pages += 1;
	}
	if(num_pages < 1 || num_pages > 4)
		return -1;
	
	int i;
	/*check whether key is already in use or not ?
		used => no need to allocated phy. space
		not used => need to allocated phy. space
	*/
	/* if used == 0 then ref_cnt must be 0
		if used == 1 then and only then ref_cnt can become more than 0
	*/
	if(keys[key].used == 0){
		//cprintf("came here with key %d & size %d\n", key, size);
		//cprintf("this is true\n");
		/*check if there is some error with setting of ref_cnt*/
		if(keys[key].ref_cnt != 0)
			return -1;
		/*not used*/
		/*everything is good to allocate phy space using kalloc() and then store this phy. addr in pages_pa global array for this key*/
		char *mem;
		//cprintf("printing the addr returned by kalloc\n");
		cprintf("printing phy. addr\n");
		for(i = 0; i < num_pages; i++){
			mem = kalloc();
			if(mem == 0){
				cprintf("kalloc failed => does not have free space\n");
				return -1;
			}
			 memset(mem, 0, PGSIZE);
			 page_pa[key].pages[i] = mem; //addr is virtual addr while using mappages, in 4th arg => we need to use V2P to convert this virt addr into phy addr. because 4th arg require phy addr. 

			cprintf("addr is %x\n", (char*)page_pa[key].pages[i]);
		}
		/*set up info in page_pa regarding phy space*/
		page_pa[key].no_of_used_pages = num_pages;
		page_pa[key].used = 1;
		keys[key].used = 1; // but till now ref_cnt is 0 , when process call shmat then we will inc ref_cnt
		keys[key].size = size;
	}
	else{
		/*used key*/
		/*if key is already in use.. means some process has already created phy side mapping, now we have to 
		check for permission and also if size of both matches or not*/
		/*should i check for ref_cnt here ? 
			like if one process has called shmget , and creeated phy space for particular key ..but he does not called shmat
			then whether we should allow other process to call shmat ?
		*/
		/*
		for above cmd i have seen linux allow this, hence I too..
		*/
		/*we will just check size here (after implementing perm , i will do that as well)*/
		if(keys[key].size != size){
			cprintf("size is wrong\n");
			return -1;
		}
		if(page_pa[key].no_of_used_pages != num_pages){
			cprintf("num_pages are  wrong\n");
			return -1;
		}
		/*should i check for used val of page_pa here ?*/
		if(page_pa[key].used == 0){
			cprintf("pages are not in used, either ref cnt has gone to 0 or something went wrong\n");
			return -1;
		}
	}
	
	
	
	
	//int x = size / PGSIZE;
	//int y = size % PGSIZE; 
	//cprintf("no of pages %d, extra offset %d, req pages %d\n", x, y, num_pages);
	return key + 10;
}

/*it should be called with id equal to the id returned by shmget*/
/*
if addr == NULL => take the space from top of userspace
otherwise 
check the addr given by user => 
	is the addr is page aligned
	is this addr is mapped
	whether it is used for something else
*/
/*for now, just use NULL wala*/
void* shmat(int id, void *addr){
		id = id - 10;
		if(id < 0 || id > 15)
			return (void*)-1;
		int i;
		struct proc *curproc = myproc();
		//the process is calling shmat for first time => then we will initialize the proc->top = KERNBASE
		for(i = 0; i < MAX_KEYS; i++){
			if(curproc->key[i] == 1)
				break;
		}
		/*it means this is first time process is calling shmat , hence we will inti proc->top = KERNBASE*/
		if(i == MAX_KEYS){
			curproc->top = KERNBASE;
		}
		
		
		
		/*now we create virtual addr space in process for that shared mem.
		nd create mapping betn va and pa using mappages*/
		/*what if same process is calling shmat on same key for more than once ?
		linux => allocate next page for that
		*/
		int num_pages = page_pa[id].no_of_used_pages;
		
		/*if curproc is calling shmat for this key for first time*/
		if(curproc->key[id] == 0){
			if((curproc->top - num_pages*PGSIZE) < curproc->sz){
				cprintf("this will trying to access already allocted space, srry don't have enough space\n");
				return (void*)-1;
			}
		
			/*take virtual addr in userspace*/
			for(i = 0; i < num_pages; i++){
				void *addr = (void*)(curproc->top - PGSIZE);
				curproc->page_va[id].pages[i] = addr;
			
				curproc->top -= PGSIZE;
			
				/*map va to pa using mappages*/
				if (mappages(curproc->pgdir, addr, PGSIZE, V2P(page_pa[id].pages[i]), PTE_P | PTE_W | PTE_U) < 0) {
					cprintf("mappages failed\n");
		    		return (void*)-1;
		  		}
		  		cprintf("va addr %x, pa addr %x\n", addr, page_pa[id].pages[i]);
			}
			curproc->page_va[id].no_of_used_pages = num_pages;
			curproc->page_va[id].used = 1;
		}
		else{
		/*A))  meaning that the curproc, has already done pa to va mapping in its virtual addr space, it has may called detached(here, we have just set the user-bit of this pages to 0), now it is calling attached again.. so(just set the user bit of this pages again)*/
		/*OR*/
		/* B))  one more possibility is that, the same proc is calling shmat with same key again, 
Here, I have 2 options,
1) I can map diff va to same pa
2) I can map same va to same pa, just give the same pointer to same proc again
Here, I have adopted 2) option
		*/
		//cprintf("before setting user bit in page curproc is alreeady using %d this %x, data is %s\n", curproc->pid, curproc->page_va[id].pages[num_pages - 1], curproc->page_va[id].pages[num_pages - 1] );
	
	
	/*This below thing should be done only for A) part*/
		for(i = 0; i < num_pages; i++){
			setpteu(curproc->pgdir, curproc->page_va[id].pages[num_pages - i - 1]);
			cprintf("curproc is  %d, va addr is %x, pa addr is %x\n", curproc->pid, curproc->page_va[id].pages[num_pages - 1], page_pa[id].pages[num_pages - 1]);
		}
		 
		 //if(curproc->key[id] == 0)
		 /*For B)), should i inc ref_cnt here also ??
		 	yes.. I am doing that.. though same proc is calling but acc to abstraction... i will inc ref_cnt (linux does the same)
		 */
		 //if(curproc->page_va[id].used == 0){
		 		(keys[id].ref_cnt)++;
		 //}
		
		curproc->page_va[id].used = 1;
		curproc->page_va[id].no_of_used_pages = num_pages;
		//cprintf("curproc is alreeady using %d this %x, data is %s\n", curproc->pid, curproc->page_va[id].pages[num_pages - 1], curproc->page_va[id].pages[num_pages - 1] );
		}
		
		if(curproc->key[id] == 0){
			(keys[id].ref_cnt)++;
			curproc->key[id] = 1;
		}
		//cprintf("currproc top pid %d is %x\n",curproc->pid, curproc->top);
		return curproc->page_va[id].pages[num_pages - 1];
}	


/*
here id should be id returned by shmget
and addr should be addr returned by shmat
*/
/*
how to verify that this id and addr are correct ?
*/
/*
dec_ref_cnt for this id
remove the mapping of this addr from virtual mem of process, but don't remove it from phy space. 
*/

/*
return val -- 
	-1 => error
	1 => success
*/
int
shmdt(int id, void *addr){
	id = id - 10;
	if(id < 0 || id > 15)
			return -1;

	struct proc *p = myproc();
	cprintf("cur pid is %d, no of used pages %d with key %d\n", p->pid, p->page_va[id].no_of_used_pages, id);
	/*have to clear all pages*/
	int i, num_pages;
	num_pages = p->page_va[id].no_of_used_pages;
	for(i = 0; i < num_pages; i++){
		clearpteu(p->pgdir, addr + (i*PGSIZE));
		//cprintf("in kernel val is %x, data is %s\n", (char*)(addr + (i*PGSIZE)),  (char*)(addr + (i*PGSIZE)));
		//p->top += PGSIZE;
	}
	p->page_va[id].used = 0;
	p->page_va[id].no_of_used_pages = 0;
	//p->key[id] = 0;
	keys[id].ref_cnt--;
	//dec_ref_cnt_of_key(id);
	return 1;
}


/*
fun -- it will clear all the shm seg of proc...
will clear all the proc side ds 
also clear the global ds which are necessary
*/

int
shm_delete_all(){
	struct proc *p = myproc();
	int i, j;
	if(p->top == KERNBASE){
		cprintf("no shm is created.. by this proc till now\n");
		return 1;
	}
	if(p->top != KERNBASE){
		deallocuvmforshm(p->pgdir, KERNBASE, p->top);
	}
	for(i = 0; i < MAX_KEYS; i++){
		if(p->key[i] == 1){
			//if(keys[i].ref_cnt == 1){//might be wrong .. this ref_cnt = 1 is might be of any other proc
				//for(j = 0; j < page_pa[i].no_of_used_pages; j++){
					//cprintf("freeing now work\n");
					//kfree(page_pa[i].pages[j]);
				//}
				page_pa[i].no_of_used_pages = 0;
				page_pa[i].used = 0;
				keys[i].used = 0;
				keys[i].size = 0;
				keys[i].ref_cnt = 0;
			//}
			//should i dec ref_cnt here if its ref_cnt is > 1 ??
		}
		p->key[i] = 0;
		for(j = 0; j < MAX_PAGES; j++){
			p->page_va[i].pages[j] = 0;
		}
		p->page_va[i].no_of_used_pages = 0;
		p->page_va[i].used = 0;
	}
	p->top = KERNBASE;
	cprintf("all done ref cnt of 0 is %d\n", keys[0].ref_cnt);
	return 1;
}

int
dec_ref_cnt_of_key(int key){
	if(key < 0 || key > 15)
		return -1;
	keys[key].ref_cnt--;
	return 0;
}


/*after proc has called, 
then we do this... meaning proc does not have its parent shared mem now,
if this is the only child we is using this shared mem.. then if ref_cnt is goes down to 0 ,
then we make that key globally unused & make its size to 0
This should also done .. only when I deleted the particular shm seg of that key...
Here.. I should clear page_pa for that key as well.. but it doesn't matter.. we never check the existance of key
using this.. hence doesn't matter(think more on this).


But we don't do this... during simple deattch as prog can call attach again.. or some other proc can also want
to access this mem.. hence we need to keep global.. key info.. 


*/

void
dec_ref_cnt(struct proc *p){
	int i;
	for(i = 0; i < MAX_KEYS; i++){
		if(p->key[i] == 1){
			if(keys[i].ref_cnt > 0)
				(keys[i].ref_cnt)--;
			//if(keys[i].ref_cnt == 0){
			//	keys[i].used = 0;
			//	keys[i].size = 0;
			//}
		}
	}
}

void
inc_ref_cnt(struct proc *p){
	int i;
	for(i = 0; i < MAX_KEYS; i++){
		if(p->key[i] == 1){
			(keys[i].ref_cnt)++;
			//if(keys[i].ref_cnt == 0){
			//	keys[i].used = 0;
			//	keys[i].size = 0;
			//}
		}
	}
}

/*
in shmat => inc ref cnt by 1
in shmdt => dec ref cnt by 1
*/
int shm_ref_cnt(int key){
	if(key < 0 || key > 15)
		return -1;
	if(keys[key].used == 0)
		return -1;
	return keys[key].ref_cnt;
}
