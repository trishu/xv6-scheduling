#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_shm_ref_cnt(void)
{
	int key;
	if(argint(0, &key) < 0)
    	return -1;
    return shm_ref_cnt(key);
}

int
sys_shmget(void)
{
	int key, size;
	if(argint(0, &key) < 0)
    	return -1;
    if(argint(1, &size) < 0)
    	return -1;
    //cprintf("in sysproc key is %d, size is %d\n", key, size);
    return shmget(key, size);
}

int
sys_shmat(void)
{
	int id;
	char *name;
	if(argint(0, &id) < 0)
    	return -1;
	if(argstr(0, &name) < 0)
		return -1;
	return (int)shmat(id, name);
}

int
sys_shm_delete_all(void)
{
	return shm_delete_all();
}


int
sys_shmdt(void)
{
	int id;
	int addr;
	if(argint(0, &id) < 0)
    	return -1;
	if(argint(1, &addr) < 0)
		return -1;
	return shmdt(id, (void*)addr);
}

int
sys_uinitlock(void)
{
	int lock;
	int name;
	if(argint(0, &lock) < 0)
		return -1;
	if(argint(1, &name) < 0)
		return -1;
	//cprintf("val of lock %x, name %x\n", lock, name);
	uinitlock((struct userspinlock*)lock, (char*)name);
	return 0;
}

int
sys_uacquire(void)
{
	int lock;
	if(argint(0, &lock) < 0)
		return -1;
	uacquire((struct userspinlock *)lock);
	return 0;
}

int
sys_urelease(void)
{
	int lock;
	if(argint(0, &lock) < 0)
		return -1;
	urelease((struct userspinlock*)lock);
	return 0;
}

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}
