# Implementation of Shared Memory in xv6

Processes can create shared memory and can communicate with each other using it. SysV type of interface is created for using shared memory. 

To use shared memory , start the xv6 using following command:

```
$ make qemu-nox
```

### Functions for handling shared memory are :
    
~~~
int shmget(int key, int size);
~~~

This function will create shared memory segment with given key and size and return the
shmid which is given as argument to shmat. In this function, we have just created 
shm segment.

~~~
void* shmat(int shmid, void* addr);
~~~

Using shmget, we have created shm segment, now attach it usign shmat, First argument 
should be shmid, which is returned by shmget. This function gives the pointer to our 
shared memory region.

~~~
int shmdt(int shmid, void* addr);
~~~

We can deattach shm segment, using shmdt function.
First argment is shmid, which is returned value of shmget.
second argument is addr, which is the returned value of shmat
On sucess, this function return 1
On failure, this function returns -1
~~~
int shm_delete_all();
~~~

We can delete all shared memory segements of process using this function. (It will
delete all shm seg. , not only one).
On sucess, return value is 1.
All other return values indicate failure

~~~
int shm_ref_cnt(int key);
~~~

This function gives the information about number of processes which are attached
to this key
First argument : key, which we have used to create this segment
return value : number indicating, no of attached processes to this key

---

### Note - 

*  Range of keys is 0 to 15

*  Maximum space created by any single key is 4 pages ie 4*4096 bytes
    

### Info -

*  Testing programs are included. Information about how to run this programs
is given in file **testing.txt**
*   **shm-test-images** folder contain the images showing results of test cases

## User-level Spinlock
Implemented User Level spinlock.
All spinlock related code is written in spinlock.c and spinlock.h files
- [ ] testing of shared memory using spinlock is remaining

