#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"


/*parent create n childreen with diff tickets
	proc are short task cpu bound proc
*/
/*
	tickets or priority are set in particular order - inc / dec	
*/
void delay(int cnt){
	int  j, k;
	for (k = 0; k < 100; k++){
		for (j = 0; j < 100000; j++){
			;
		}
		yield();
	}
}


int main(int argc, char *argv[]){
	int n;
	if(argc < 2)
		n = 1;
	else if(argc == 2)
		n = atoi(argv[1]);
	else{
		printf(1, "usage : procname no_of_proc\n");
		exit();
	}	

	int pids[n];
	int rets[n];
	int ticket[n];
	
	
	#ifdef PRIORITY
		setpriority(getpid(), 1);
	#else
	#ifdef MULTILEVEL
		setpriority(getpid(), 1);
	#endif
	#endif
	for(int i = 0; i < n; i++){
		//set tickets
		#ifdef LOTTERY
			int tickets = 10*i  + 20;
		#else
		#ifdef PRIORITY
			int priority = 10 + i*2;
		#else
		#ifdef MULTILEVEL
			int priority = i % 3 + 1;
		#endif
		#endif
		#endif

		int id = fork();
		if(id == 0){ //child
			#ifdef LOTTERY
				settickets(getpid(), tickets);
			#else
			#ifdef PRIORITY
				setpriority(getpid(), priority);
			#else
			#ifdef MULTILEVEL
				setpriority(getpid(), priority);
			#endif
			#endif
			#endif		
			delay(20);
			exit();
		}
		else if(id > 0){ //parent
			pids[i] = id;
			#ifdef LOTTERY
				ticket[i] = tickets;
			#else
			#ifdef PRIORITY
				ticket[i] = priority;
			#else
			#ifdef MULTILEVEL
				ticket[i] = priority;
			#endif
			#endif
			#endif
		}
		else{
			printf(1, "fork failed\n");
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		rets[i] = wait(); 
	}
	
	printf(1, "all children terminated\n");
	for(int i = 0; i < n; i++){
		#ifdef LOTTERY
			printf(1, "child %d, pid %d, tickets %d\n", i, pids[i], ticket[i]);
		#else
		#ifdef PRIORITY
			printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
		#else
		#ifdef MULTILEVEL
			printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
		#endif
		#endif
		#endif			
	}

	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n; i++)
		printf(1, "pid %d\n", rets[i]);
	
	exit();
}
