#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

/* This program should create n children which in turn create m children 
*/

void delay(){
	int  j, k;
	for (k = 0; k < 1000; k++){
		for (j = 0; j < 100000; j++){
			;
		}
		yield();
	}
}


int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	int pids[n+n*m];
	int rets[n+n*m];
	int ticket[n+n*m];
	int pk = 0, rk = 0, tk = 0;

	int curpid = getpid();
	#ifdef PRIORITY
		setpriority(curpid, 1);
	#else
	#ifdef MULTILEVEL
		setpriority(curpid, 1);
	#endif
	#endif	
	
	for(int i = 0; i < n; i++){
		//set tickets
		#ifdef LOTTERY
			int tickets = 15;
		#else
		#ifdef PRIORITY
			int priority  = 11;
		#else
		#ifdef MULTILEVEL
			int priority  = 1;
		#endif
		#endif
		#endif
		int id = fork();
		if(id == 0){ //child
			int ppid = getpid();
			#ifdef PRIORITY
				printf(1, "pid %d, ppid %d, priority %d\n", ppid, curpid, priority);
				setpriority(ppid, priority);
			#else
			#ifdef LOTTERY
				printf(1, "pid %d, ppid %d, tickets %d\n", ppid, curpid, tickets);
				settickets(ppid, tickets);
			#else
			#ifdef MULTILEVEL
				printf(1, "pid %d, ppid %d, priority %d\n", ppid, curpid, priority);
				setpriority(ppid, priority);
			#endif
			#endif
			#endif				
			for(int j = 0; j < m; j++){
				#ifdef LOTTERY
					int t = 10*j +  5*i + 20;
				#else
				#ifdef PRIORITY
					int p = 10 + 2*j + 3*i;
				#else
				#ifdef MULTILEVEL
					int p = (m - j) % 3 + 1;
				#endif
				#endif
				#endif
				int pid = fork();
				if(pid == 0){
					#ifdef LOTTERY
						printf(1, "pid %d, ppid %d, tickets %d\n", getpid(), ppid,  t);
						settickets(getpid(), t);
					#else
					#ifdef PRIORITY
						printf(1, "pid %d, ppid %d, priority %d\n", getpid(), ppid,  p);
						setpriority(getpid(), p);
					#else
					#ifdef MULTILEVEL
						printf(1, "pid %d, ppid %d, priority %d\n", getpid(), ppid,  p);
						setpriority(getpid(), p);
					#endif
					#endif
					#endif
					delay();
					exit();
				}
			}
			for(int j = 0; j < m; j++){
					int ret = wait();
					printf(1, "wait is over for child of child %d\n", ret);
			}
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		int ret = wait(); 
		printf(1, "wait is over for child %d\n", ret);
	}
		

	
	exit();
}
