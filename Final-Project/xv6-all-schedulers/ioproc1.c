#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

/*one parent is creating n no of childrens 
	process are io bound proc
*/
/*
	progname no_of_proc
	progname
*/

/*multilevel .. with large no of proc ... does not work very well*/
int main(int argc, char *argv[]){
	int i, id, k, n, rand = 0;
	int retime, rutime, stime;
	struct processInfo info;
    int pid;
	double x = 0, z, d = 1.0;
	if(argc < 2)
		n = 1;
	else if(argc == 2)
		n = atoi(argv[1]);
	else{
		printf(1, "Usage: programname   no_of_proc\n");
		exit();
	}
	#ifdef LOTTERY
		settickets(getpid(), 12);
	#else
	#ifdef PRIORITY
		setpriority(getpid(), 1);
	#else
	#ifdef MULTILEVEL
		setpriority(getpid(), 1);
	#endif
	#endif
	#endif
	printf(1, "no is %d\n", n);
	for(i = 0; i < n; i++){
		id = fork();
		if(id == 0){
			#ifdef MULTILEVEL
				int p = (i) % 3 + 1;
				setpriority(getpid(), p);
				printf(1, "pid %d with priority %d\n", getpid(), p);
				sleep(10);
				for(k = 0; k < 100; k++){
					for (z = 0; z < 8000.0; z+= d){
						x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
					}
					sleep(1);
				}
			#else
			#ifdef PRIORITY
				int p = 10 + 2*(i + 2);
				setpriority(getpid(), p);
				sleep(10);
				printf(1, "pid %d with priority %d\n", getpid(), p);
				for(k = 0; k < 150; k++){
					for (z = 0; z < 1000.0; z+= d){
						x =  x + 3.14 * 89.64;  
					}
					sleep(1);
				}
			#else
			#ifdef LOTTERY
				int t = 10*(i + 2);
				settickets(getpid(), t);
				sleep(10);
				printf(1, "pid %d with tickets %d\n", getpid(), t);			
				for(k = 0; k < 150; k++){
					for (z = 0; z < 800000.0; z+= d){
						x =  x + 3.14 * 89.64;  
					}
					sleep(1);
				}
			#endif
			#endif
			#endif
			break;
		}
	}
	for(i = 0; i < n; i++){
		pid = wait2(&retime, &rutime, &stime);
		if(pid != -1)
			printf(1, "pid %d, ready time %d, running time %d,  sleeping time %d\n", pid, retime, rutime, stime);
	}
	exit();
}
