#include "types.h"
#include "stat.h"
#include "user.h"
#include "processInfo.h"
int main(int argc, char *argv[])
{
    struct processInfo info;
    int pid;
    setpriority(getpid(), 1);
    printf(1, "PID\tParent\ttickets\tpriority\tstate\tName\tctime\tNo of Context Swt\n");
    for(int i=1; i<=getmaxpid(); i++)
    {
        pid = i;
        if(getprocinfo(pid, &info) == 0)
	  printf(1, "%d\t%d\t%d\t%d\t%s\t%s\t%d\t%d\n", info.pid, info.parent, info.tickets, info.priority, info.state, info.pname, info.ctime, info.numberContexSwitches);
    }
    exit();
}
