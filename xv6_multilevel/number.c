#include "types.h"
#include "stat.h"
#include "user.h"


int main(int argc, char *argv[]){
  int i, start, end;

  if(argc < 3){
    printf(1, "Usage : ./a.out start end");
    exit();
  }
	start = atoi(argv[1]);
	end = atoi(argv[2]);
	for(i = start; i <= end; i++){
		printf(1,"%d ", i);
	}
	printf(1,"\n");
  exit();
}
