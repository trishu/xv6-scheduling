struct processInfo
{
    int pid;
    int parent;
    int tickets;
    int priority;
    int psize;
    char pname[16];
	int state;
    int numberContexSwitches;
};
