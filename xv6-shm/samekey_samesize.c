#include "types.h"
#include "stat.h"
#include "user.h"

/*
same proc is calling shmget and shmat, with samekey and samesize, 
it will get same physical space, as well as same virtual space
*/

int main(int argc, char *argv[]){
	int key = 4;
	int size = 4096;
	char c;
	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm);
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s = shm;
	for (c = 'A'; c <= 'O'; c++)
		*s++ = c;
	*s = 0;
	
	printf(1, "writeen data is %s\n", shm);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));

printf(1, "-----------------------------------------------------------------------------------\n");
/*calling shmget & shmat by same process with same key and size
should map to same phy space, and even virtual space should remain same.
*/

	int id1 = shmget(key, size);
	if(id1 < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm1 = (char*)shmat(id1, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm1);
	if(shm1 == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s1 = shm1;
	for (c = 'A'; c <= 'G'; c++)
		*s1++ = c;
	*s1 = 0;
	
	printf(1, "writeen data is %s\n", shm1);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	
	printf(1, "--------------------------------------------------------\n");
	printf(1, "Hence, we can both shmseg has same virtual and same physical addr\n");

	exit();
}
