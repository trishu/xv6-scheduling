#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	if(argc < 2){
		printf(1, "Usage : program_name key\n");
    	exit();
	}
	int key = atoi(argv[1]);
	printf(1, "key %d\n", key);
	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	exit();
}
