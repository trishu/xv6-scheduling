#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	int key = 7;
	int size = 4096;
	char c;
	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm);
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s = shm;
	for (c = 'A'; c <= 'M'; c++)
		*s++ = c;
	*s = 0;
	
	printf(1, "writeen data is %s\n", shm);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	
	int pid = fork();
	if(pid == 0){
		printf(1, "----------------------------\n");
		printf(1, "i am child with pid %d\n", getpid());
		printf(1, "able to read data what my parent has written to shm seg without calling shmget or shmat\n");
		printf(1, "meaning child inherits shm seg\n");
		
		printf(1, "data reading %s\n", shm);
		sleep(15);
		printf(1, "child is mapped to same phy. space , hence changed data is %s\n", shm);
		
		printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
		exit();
	}
	else{
		sleep(10);
		printf(1, "------------------------\n");
		printf(1, "parent ---> changing some data in shm\n");
		*shm = '*';
		printf(1, "data changed by parent %s\n", shm);
		printf(1, "------------------------\n");
		wait();
		printf(1, "-----------------------------\n");
		printf(1,"parent waiting for child\n");
		
	}
	exit();
}
