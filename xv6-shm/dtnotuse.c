#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	int key = 10;
	int size = 100;
	char c;
	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm);
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s = shm;
	for (c = 'A'; c <= 'Z'; c++)
		*s++ = c;
	*s = 0;
	
	printf(1, "writeen data is %s\n", shm);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	
	int x = shmdt(id, shm);
	if(x == -1){
		printf(1, "shmdt failed, either id is wrong.. or something went wrong\n");
		exit();
	}
	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	printf(1, "This seg is deattched successfully!!\n");
	printf(1, "data should not be read after deattching , trap 14 is generead %s\n", shm);
	
	exit();
}
