#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	if(argc != 3){
		printf(1, "Usage: program_name   key   size\n");
		exit();
	}
	int key = atoi(argv[1]);
	int size = atoi(argv[2]);

	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm);
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	
	printf(1, "reading data from shared mem  %s\n", shm);
	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
	
	printf(1, "-------------------------------------------------------------\n");
	printf(1, "Shared mem segment remains even though process terminates, hence data written by writeshm proc can be read by readshm proc\n");
	exit();
}
