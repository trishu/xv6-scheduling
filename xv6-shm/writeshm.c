#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]){
	if(argc != 3){
		printf(1, "Usage: program_name   key   size\n");
		exit();
	}
	int key = atoi(argv[1]);
	int size = atoi(argv[2]);
	char c;
	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm);
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s = shm;
	for (c = 'A'; c <= 'Z'; c++)
		*s++ = c;
	*s = 0;
	
	printf(1, "writeen data is %s\n", shm);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));
		
	exit();
}
