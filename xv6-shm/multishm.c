#include "types.h"
#include "stat.h"
#include "user.h"

/*creating 3 shm seg by same process, with diff key, diff size .... diff key and same size is also tried with seg 2 and seg 3*/

int main(int argc, char *argv[]){
	int key = 1;
	int size = 51;
	char c;
	int id = shmget(key, size);
	if(id < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm = (char*)shmat(id, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm);
	if(shm == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s = shm;
	for (c = 'A'; c <= 'O'; c++)
		*s++ = c;
	*s = 0;
	
	printf(1, "writeen data is %s\n", shm);

	printf(1, "No of ref to key %d are %d\n", key, shm_ref_cnt(key));

printf(1, "-----------------------------------------------------------------------------------\n");
	
	int key1 = 2;
	int size1 = 100;
	int id1 = shmget(key1, size1);
	if(id1 < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm1 = (char*)shmat(id1, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm1);
	if(shm1 == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s1 = shm1;
	for (c = 'A'; c <= 'G'; c++)
		*s1++ = c;
	*s1 = 0;
	
	printf(1, "writeen data is %s\n", shm1);

	printf(1, "No of ref to key %d are %d\n", key1, shm_ref_cnt(key1));


printf(1, "-----------------------------------------------------------------------------------\n");
	
	int key2 = 3;
	int size2 = 100;
	int id2 = shmget(key2, size2);
	if(id1 < 0){
		printf(1, "shmget failed\n");
		exit();
	}
	
	char *shm2 = (char*)shmat(id2, "mahi");
	printf(1, "returned addr of shmat is %x\n", shm2);
	if(shm2 == (void*)-1){
		printf(1, "shmat failed\n");
		exit();
	}
	char *s2 = shm2;
	for (c = 'A'; c <= 'K'; c++)
		*s2++ = c;
	*s2 = 0;
	
	printf(1, "writeen data is %s\n", shm2);

	printf(1, "No of ref to key %d are %d\n", key2, shm_ref_cnt(key2));


	printf(1, "-----------------------------------------------------\n");
	printf(1, "Same proc can create various shared mem seg with diff key and diff sizes... Here, limit to per proc shm seg. is 16\n");


	exit();
}
