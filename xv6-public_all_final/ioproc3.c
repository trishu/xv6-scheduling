#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"


/*parent create n childreen with diff tickets
	proc are io bound proc
*/
void delay(int cnt){
	int k;
	//double x = 0, z, d = 1.0;
	for(k = 0; k < 1000; k++){
		/*for (z = 0; z < 80000.0; z+= d){
			x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
		}*/
		sleep(1);
	}
}


int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	int pids[n];
	int rets[n];
	int ticket[n];
	
	//settickets(getpid(), 10);
	setpriority(getpid(), 1);
	for(int i = 0; i < n; i++){
		//set tickets
		//int tickets = 10*i  + 20;
		int priority = i % 3 + 1;
		int id = fork();
		if(id == 0){ //child
			//settickets(getpid(), tickets);
			setpriority(getpid(), priority);
			delay(20);
			exit();
		}
		else if(id > 0){ //parent
			pids[i] = id;
			//ticket[i] = tickets;
			ticket[i] = priority;
		}
		else{
			printf(1, "fork failed\n");
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		rets[i] = wait(); 
	}
	
	printf(1, "all children terminated\n");
	for(int i = 0; i < n; i++)
		//printf(1, "child %d, pid %d, tickets %d\n", i, pids[i], ticket[i]);
		printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
	
		
	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n; i++)
		printf(1, "pid %d\n", rets[i]);
	
	exit();
}
