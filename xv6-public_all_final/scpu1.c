#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

/*parent create n proc which in turn create m proc each 
	proc are cpu bound processes 
*/
/*
settickets are done in this proc only
use --> procinfo for testing no of cont swts
*/

int main(int argc, char *argv[]){
	int i, id, j, id1, p, q;
	if(argc < 3){
		printf(1, "Usage: programname   no_of_proc   no_of_proc\n");
		exit();
	}
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	printf(1, "no is %d\n", n);
	printf(1, "no is %d\n", m);
	for(i = 0; i < n; i++){
		id = fork();
		if(id == 0){
			printf(1, "child %d created\n", getpid());
			for(j = 0; j < m; j++){
				id1 = fork();
				if(id1 == 0){
					settickets(getpid(), 10*(j + 2) + 50*(i));
					printf(1, "child %d created", getpid());
					for (p = 0; p < 100000; p++){
						for (q = 0; q < 10000000; q++){
							;
						}
						yield();
					}
					printf(1,"came here %d", getpid());
					break;
				}
			}
			printf(1,"came here %d", getpid());
			break;
		}
	}
	for(i = 0; i < n + n*m; i++)
		wait();
	exit();
}
