#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"


/*parent create n childreen with diff tickets
	proc are short task cpu bound proc
*/
/*
	tickets or priority are set in particular order - inc / dec	
*/
void delay(int cnt){
	int  j, k;
	for (k = 0; k < 100; k++){
		for (j = 0; j < 100000; j++){
			;
		}
		yield();
	}
}


int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	int pids[n];
	int rets[n];
	int ticket[n];
	
	//settickets(getpid(), 10);
	setpriority(getpid(), 1);
	for(int i = 0; i < n; i++){
		//set tickets
		//int tickets = 10*i  + 20;
		//int priority = 10 + i*2;
		int priority = i % 3 + 1;
		/* 		random tickets & prio
		int tickets;
		if(i % 2 == 0)
			tickets = 10*i  + 20*(n - i) - 100;
		else
			tickets = 10*i  + 20*(n - i) + 100;
		int priority;
		if(i % 2 == 0)
			priority = 10 + 2*i + 2;
		else
			priority =  10 + 2*i - 2;*/

		int id = fork();
		if(id == 0){ //child
			//settickets(getpid(), tickets);
			setpriority(getpid(), priority);
			delay(20);
			exit();
		}
		else if(id > 0){ //parent
			pids[i] = id;
			//ticket[i] = tickets;
			ticket[i] = priority;
		}
		else{
			printf(1, "fork failed\n");
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		rets[i] = wait(); 
	}
	
	printf(1, "all children terminated\n");
	for(int i = 0; i < n; i++)
		//printf(1, "child %d, pid %d, tickets %d\n", i, pids[i], ticket[i]);
		printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);

	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n; i++)
		printf(1, "pid %d\n", rets[i]);
	
	exit();
}
