#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

/*one parent is creating n no of childrens 
	process are short task based cpu bound processes
*/
/*
use=> settickets --> to set tickets to particular proc
use => setpriority --> to set priority to particular proc
use => procinfo --> to see no of cont swt of proc
*/
/*
	here, now .. tickets and prio are setup randomly
*/
int main(int argc, char *argv[]){
	int i, n, id, k, j;
	if(argc < 2)
		n = 1;
	else if(argc == 2)
	 	n = atoi(argv[1]);
	 else{
	 	printf(1, "Usage : programname   no_of_proc\n");
	 	exit();
	 }
	printf(1, "no is %d\n", n);
	for(i = 0; i < n; i++){
		id = fork();
		if(id == 0){
			/*if(i % 2 == 0)
				settickets(getpid(), 10*(i + 2) + 5*(i + 4) + 50);
			else
				settickets(getpid(), 10*(i + 2) + 5*(i + 4) - 50);*/
			if(i % 2 == 0)
				setpriority(getpid(), 10*(i + 2) + 5*(i + 4) + 50);
			else
				setpriority(getpid(), 10*(i + 2) + 5*(i + 4) - 50);
			printf(1, "%d creating child\n", getpid());
			for(k = 0; k < 100000; k++){
				for(j = 0; j < 1000000000; j++){
					;
				}
				yield();
			}
			
			printf(1,"came here");
			break;
		}
	}
	for(i = 0; i < n; i++)
		wait();
	exit();
}
