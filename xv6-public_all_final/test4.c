#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

/* This program should create n children which in turn create m children 
*/

void delay(int cnt){
	int i, j, k;
    int *data;

    data = (int *)malloc(sizeof(int) * 1024 * 10);
    if (data <= 0)
        printf(1, "Error on memory allocation \n");

    for (i = 0; i < cnt; i++)
    {
        for (k = 0; k < 5700; k++)
            for (j = 0; j < 1024 * 10; j++)
                data[j]++;
    }
}


int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	int pids[n+n*m];
	int rets[n+n*m];
	int ticket[n+n*m];
	int pk = 0, rk = 0, tk = 0;

	int curpid = getpid();
	//settickets(curpid, 10);
	setpriority(curpid, 1);
	
	for(int i = 0; i < n; i++){
		//set tickets
		//int tickets = 15;
		int priority  = 1;
		int id = fork();
		if(id == 0){ //child
			int ppid = getpid();
			printf(1, "pid %d, ppid %d, priority %d\n", ppid, curpid, priority);
			setpriority(ppid, priority);
			//settickets(ppid, tickets);
			for(int j = 0; j < m; j++){
				//int t = 10*j +  5*i + 20;
				int p = j % 3 + 1;
				int pid = fork();
				if(pid == 0){
					//printf(1, "pid %d, ppid %d, tickets %d\n", getpid(), ppid,  t);
					//settickets(getpid(), t);
					printf(1, "pid %d, ppid %d, priority %d\n", getpid(), ppid,  p);
					setpriority(getpid(), p);
					delay(20);
					exit();
				}
			}
			for(int j = 0; j < m; j++){
					//rets[rk++] = wait();
					int ret = wait();
					printf(1, "wait is over for child of child %d\n", ret);
			}
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		//rets[rk++] = wait();
		int ret = wait(); 
		printf(1, "wait is over for child %d\n", ret);
	}
	
	/*printf(1, "all children terminated\n");
	for(int i = 0; i < n + n*m; i++)
		printf(1, "child %d, pid %d, tickets %d\n", i, pids[i], ticket[i]);*/
		
		
/*	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n + n*m; i++)
		printf(1, "pid %d\n", rets[i]);*/
	

	
	exit();
}
