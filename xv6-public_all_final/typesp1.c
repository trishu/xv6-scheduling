#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

/*parent create n childreen with diff tickets
	proc are io bound proc
*/
int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	    struct processInfo info;
	int pids[n];
	int rets[n];
	int ticket[n];
	int j, k, p;
	int pid;
	int retime, rutime, stime;
	//settickets(getpid(), 10);
	setpriority(getpid(), 1);
	for(int i = 0; i < n; i++){
		//set tickets
		j = i % 3;
		//int tickets;
		int priority;
		int id = fork();
		if(id == 0){ //child
			j = (getpid() - 4) % 3;
			switch(j) {
				case 0: //CPU‐bound process (CPU):
					//tickets = 100 + i * 100;
					//settickets(getpid(), tickets);
					priority = 5;
					setpriority(getpid(), priority);
					for(double z = 0; z < 10000.0; z+= 0.1){
				         double x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
					}
					break;
				case 1: //short tasks based CPU‐bound process (S‐CPU):			
				    //tickets = 50;
					//settickets(getpid(), tickets);
					priority = 10;
					setpriority(getpid(), 2);
					for (k = 0; k < 100; k++){
						for (j = 0; j < 1000000; j++){
						;
						}
						yield();
					}
					break;
				case 2:// simulate I/O bound process (IO)
					//tickets = 20;
					//settickets(getpid(), tickets);
					priority = 15;
					setpriority(getpid(), priority);
					for(k = 0; k < 100; k++){
						sleep(1);
					}
					break;
			}
	if(getprocinfo(getpid(), &info) == 0)
		  printf(1, "pid %d\tppid %d\t tickets %d\tstate %s\t pname %s\tctime %d\tcntsw %d\n", info.pid, info.parent, info.tickets, info.state, info.pname, info.ctime, info.numberContexSwitches);
			exit();
		}
		else if(id > 0){ //parent
			pids[i] = id;
			p = (id - 4)%3;
			if(p == 0)
				//ticket[i] = 100 + i *100;
				ticket[i] = 5;
			else if(p == 1)
				//ticket[i] = 50;
				ticket[i] = 10;
			else 
				//ticket[i] = 20;
				ticket[i] = 15;
		}
		else{
			printf(1, "fork failed\n");
			exit();
		}
		continue;
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		pid =  wait2(&retime, &rutime, &stime);
		rets[i] = pid;
		int r  = (pid - 4) % 3;
		switch(r) {
			case 0: // CPU bound processes
				printf(1, "CPU-bound, pid: %d, ready: %d, running: %d, sleeping: %d, turnaround: %d\n", pid, retime, rutime, stime, retime + rutime + stime);
					break;
			case 1: // CPU bound processes, short tasks
				printf(1, "CPU-S bound, pid: %d, ready: %d, running: %d, sleeping: %d, turnaround: %d\n", pid, retime, rutime, stime, retime + rutime + stime);
				break;
			case 2: // simulating I/O bound processes
				printf(1, "I/O bound, pid: %d, ready: %d, running: %d, sleeping: %d, turnaround: %d\n", pid, retime, rutime, stime, retime + rutime + stime);
				break;
		}

	}
	
	printf(1, "all children terminated\n");
	for(int i = 0; i < n; i++)
		//printf(1, "child %d, pid %d, tickets %d\n", i, pids[i], ticket[i]);
		printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
		
	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n; i++)
		printf(1, "pid %d\n", rets[i]);
	
	exit();
}
