#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

/* this process create n children proc
	proc type = cpu bound proc => doing some dummy calculations and looping conti.
	use => settickets syscall => to change the tickets of particular proc
	use => setpriority syscall => to change the prio of proc
	use => procinfo syscall --> to see all runnable proc .. and their no of context swts
*/
/*
	1) create 2 runnable proc of same parent & test using procinfo
	2) create 5 runnable proc of same parent & test using procinfo
	3) create 10 runnable proc of same parent & test using procinfo
*/
/*
	LOTTERY => more tickets --> more context swts
	PRIORITY => less val of prio --> higher priority 
	simple priority ==> default = 10 ... lower value of prio proc will run first ..& then higher prio proc will proceed
	multilevel ==> 3 queue : 1, 2, 3 ...default = 2
							first , all proc in queue 1 will get chance 
							then if there are no proc in queue 1 then ..proc in queue 2 will get chance.
							then if there are no proc in queue 1 & 2 ..then proc in queue 3 will get chance.
*/

int main(int argc, char *argv[]){
	int i, id, n;
	double x = 0, z, d = 1.0;
	if(argc < 2)
		n = 1;
	else if(argc == 2)
		n = atoi(argv[1]);
	else{
		printf(1, "usage : procname no_of_proc\n");
		exit();
	}	
	printf(1, "no is %d\n", n);
	for(i = 0; i < n; i++){
		id = fork();
		if(id == 0){
			setpriority(getpid(), 12);
			printf(1, "%d creating child\n", getpid());
			for(z = 0; z < 8000000000.0; z +=d){
				x = x + 3.14 * 89.64;
			}
			printf(1,"came here");
			break;
		}
	}
	for(i = 0; i < n; i++)
		wait();
	exit();
}
