#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"


/*parent create n childreen with diff tickets
	proc are doing delay .. in malloc and some data filling 
	in mem
*/
/*
 LOTTERY =>> assigning tickets  to procs in inc. order. 
 PRIORITY =>> assigning prio values to procs in inc. order
 MULTILEVEL ==> assigning proc to queue at random
*/
/*
	order of exit of proc is printed
	LOTTERY => higher ticket proc will terminate first
	PRIORITY => lower value of priority proc i.e proc with higher prio will terminate first
	MULTILEVEL => all procs in lower numbered queue ie. higher prio queue will terminate first
*/
void delay(int cnt){
	int i, j, k;
    int *data;

    data = (int *)malloc(sizeof(int) * 1024 * 10);
    if (data <= 0)
        printf(1, "Error on memory allocation \n");

    for (i = 0; i < cnt; i++)
    {
        for (k = 0; k < 5700; k++)
            for (j = 0; j < 1024 * 10; j++)
                data[j]++;
    }
}


int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	int pids[n];
	int rets[n];
	int ticket[n];
	
	//settickets(getpid(), 10);
	setpriority(getpid(), 1);
	
	for(int i = 0; i < n; i++){
		//set tickets
		//int tickets = 10*i  + 20;
		//int priority = 10 + 2*i;
		int priority = i % 3 + 1;
		/*setting random val*/
		/*int tickets;
		if(i % 2 == 0)
			tickets = 10*i  + 20*(n - i) - 100;
		else
			tickets = 10*i  + 20*(n - i) + 100;
		int priority;
		if(i % 2 == 0)
			priority = 10 + 2*i + 2;
		else 
			priority = 10 + 2*i - 2;*/

		int id = fork();
		sleep(10);
		if(id == 0){ //child
			//settickets(getpid(), tickets);
			//setpriority(getpid(), priority);
			delay(20);
			exit();
		}
		else if(id > 0){ //parent
			pids[i] = id;
			ticket[i] = priority;
		}
		else{
			printf(1, "fork failed\n");
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		rets[i] = wait(); 
	}
	
	printf(1, "all children terminated\n");
	for(int i = 0; i < n; i++)
		printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
		
	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n; i++)
		printf(1, "pid %d\n", rets[i]);
	
	exit();
}
