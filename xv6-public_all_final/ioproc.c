#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

/*one parent is creating n no of childrens 
	process are io bound proc
*/
int main(int argc, char *argv[]){
	int i, id, k, n;
	int retime, rutime, stime;
	struct processInfo info;
    int pid;
	//double x = 0, z, d = 1.0;
	if(argc < 2)
		n = 1;
	else if(argc == 2)
		n = atoi(argv[1]);
	else{
		printf(1, "Usage: programname   no_of_proc\n");
		exit();
	}
	printf(1, "no is %d\n", n);
	for(i = 0; i < n; i++){
		id = fork();
		if(id == 0){
			settickets(getpid(), 10*(i + 2) + 50*(i + 1));
			printf(1, "%d creating child\n", getpid());
			for(k = 0; k < 500; k++){
				/*for (z = 0; z < 800000.0; z+= d){
					x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
				}*/
				sleep(1);
			}
			if(getprocinfo(getpid(), &info) == 0)
				printf(1, "%d\t%d\t%d\t%d\t%s\t%s\t%d\t%d\n", info.pid, info.parent, info.tickets, info.priority, info.state, info.pname, info.ctime, info.numberContexSwitches);

			//printf(1,"came here");
			break;
		}
	}
	for(i = 0; i < n; i++){
		//pid = wait();
		pid = wait2(&retime, &rutime, &stime);
		if(pid != -1)
			printf(1, "pid %d, ready time %d, running time %d,  sleeping time %d\n", pid, retime, rutime, stime);
	}
		//wait();
	exit();
}
