#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

/*parent create n proc which in turn create m proc each 
	proc are cpu bound processes 
*/

int main(int argc, char *argv[]){
	int i, id, j, id1;
	double x = 0, z, d = 1.0;
	if(argc < 3){
		printf(1, "Usage: programname   no_of_proc   no_of_proc\n");
		exit();
	}
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	printf(1, "no is %d\n", n);
	printf(1, "no is %d\n", m);
	for(i = 0; i < n; i++){
		id = fork();
		//sleep(5);
		if(id == 0){
			setpriority(getpid(), 11);
			printf(1, "child %d created\n", getpid());
			for(j = 0; j < m; j++){
				id1 = fork();
				//sleep(10);
				//if(i == 1)
				//	sleep(10);
				//else
				//	sleep(20);
				if(id1 == 0){
					setpriority(getpid(), 12);
					printf(1, "child %d created", getpid());
					for(z = 0; z < 8000000000.0; z +=d){
						x = x + 3.14 * 89.64;
					}
					printf(1,"came here %d", getpid());
					break;
				}
			}
			printf(1,"came here %d", getpid());
			break;
		}
	}
	for(i = 0; i < n + n*m; i++)
		wait();
	exit();
}
