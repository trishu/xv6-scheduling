struct processInfo
{
    int pid;
    int parent;
    int tickets;
    int priority;
    char state[10];
    char pname[16];
    int ctime;
    int numberContexSwitches;
};
