#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"



/* this process create n children proc
	proc type = cpu bound proc => doing some dummy calculations and looping conti.
	use => settickets syscall => to change the tickets of particular proc
	use => setpriority syscall => to change the prio of proc
	use => procinfo syscall --> to see all runnable proc .. and their no of context swts
*/
/*
	1) create 2 runnable proc of same parent & test using procinfo
	2) create 5 runnable proc of same parent & test using procinfo
	3) create 10 runnable proc of same parent & test using procinfo
*/
/*
	LOTTERY => more tickets --> more context swts
	PRIORITY => less val of prio --> higher priority 
	simple priority ==> default = 10 ... lower value of prio proc will run first ..& then higher prio proc will proceed
	multilevel ==> 3 queue : 1, 2, 3 ...default = 2
							first , all proc in queue 1 will get chance 
							then if there are no proc in queue 1 then ..proc in queue 2 will get chance.
							then if there are no proc in queue 1 & 2 ..then proc in queue 3 will get chance.
*/


/*parent create n childreen with diff tickets
	proc are doing delay .. in malloc and some data filling 
	in mem
*/
/*
 LOTTERY =>> assigning tickets  to procs in inc. order. 
 PRIORITY =>> assigning prio values to procs in inc. order
 MULTILEVEL ==> assigning proc to queue at random
*/
/*
	order of exit of proc is printed
	LOTTERY => higher ticket proc will terminate first
	PRIORITY => lower value of priority proc i.e proc with higher prio will terminate first
	MULTILEVEL => all procs in lower numbered queue ie. higher prio queue will terminate first
*/
void delay(int cnt){
	int i, j, k;
    int *data;

    data = (int *)malloc(sizeof(int) * 1024 * 10);
    if (data <= 0)
        printf(1, "Error on memory allocation \n");

    for (i = 0; i < cnt; i++)
    {
        for (k = 0; k < 5700; k++)
            for (j = 0; j < 1024 * 10; j++)
                data[j]++;
    }
}


int main(int argc, char *argv[]){
	int n;
	if(argc < 2)
		n = 1;
	else if(argc == 2)
		n = atoi(argv[1]);
	else{
		printf(1, "usage : procname no_of_proc\n");
		exit();
	}	

	int pids[n];
	int rets[n];
	int ticket[n];
	
	#ifdef LOTTERY
		settickets(getpid(), 10);
	#else
	#ifdef PRIORITY
		setpriority(getpid(), 1);
	#else
	#ifdef MULTILEVEL
		setpriority(getpid(), 1);
	#endif
	#endif
	#endif
	
	for(int i = 0; i < n; i++){
		//set tickets
		#ifdef LOTTERY
			int tickets = 10*i  + 20;
		#else
		#ifdef PRIORITY
			int priority = 10 + 2*i;
		#else
		#ifdef MULTILEVEL
			int priority = i % 3 + 1;
		#endif
		#endif
		#endif
		/*setting random val*/
		/*int tickets;
		if(i % 2 == 0)
			tickets = 10*i  + 20*(n - i) - 100;
		else
			tickets = 10*i  + 20*(n - i) + 100;
		int priority;
		if(i % 2 == 0)
			priority = 10 + 2*i + 2;
		else 
			priority = 10 + 2*i - 2;*/

		int id = fork();
		sleep(10);
		if(id == 0){ //child
			#ifdef LOTTERY
				settickets(getpid(), tickets);
				delay(10);
			#else
			#ifdef PRIORITY
				setpriority(getpid(), priority);
				delay(10);
			#else
			#ifdef MULTILEVEL
				setpriority(getpid(), priority);
				sleep(10);
				delay(5);
			#endif
			#endif
			#endif
			exit();
		}
		else if(id > 0){ //parent
			pids[i] = id;
			#ifdef LOTTERY
				ticket[i] = tickets;
			#else
			#ifdef PRIORITY
				ticket[i] = priority;
			#else
			#ifdef MULTILEVEL
				ticket[i] = priority;
			#endif
			#endif
			#endif
		}
		else{
			printf(1, "fork failed\n");
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		rets[i] = wait(); 
	}
	
	printf(1, "all children terminated\n");
	for(int i = 0; i < n; i++){
		#ifdef LOTTERY
			printf(1, "child %d, pid %d, tickets %d\n", i, pids[i], ticket[i]);
		#else
		#ifdef PRIORITY
			printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
		#else
		#ifdef MULTILEVEL
			printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);		
		#endif
		#endif
		#endif
	}
		
	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n; i++)
		printf(1, "pid %d\n", rets[i]);
	
	exit();
}
