## Scheduling Algorithms in xv6

Three Scheduling Algorithms are added in xv6
- [x] Lottery Scheduler
- [x] Priority Scheduler
- [x] Multilevel Priority Scheduler

---

### Lottery Scheduler

Lottery scheduling is a probabilistic scheduling algorithm.Processes are each assigned some number of lottery tickets, and the scheduler draws a random ticket
to select the next process. The distribution of tickets need not be uniform; granting a process more tickets provides it a relatively higher chance of selection. 
Lottery scheduling solves the problem of starvation. Giving each process at least one lottery ticket guarantees that it has a non-zero probability of being 
selected at each scheduling operation. Default tickets given to each process at start are 10. You can use setTickets program to set the particular number of 
tickets to particular process.


To run **LOTTERY Scheduler**, use following command when compiling xv6:

``` 
$ make qemu-nox SCHEDPOLICY=LOTTERY 
```

---

### Priority Scheduler

The priority scheduling algorithm  represents a preemptive policy that executes processes based on their priority. The scheduling policy first select the 
runnable process with the lowest value of priority, meaning highest priority and executes it, after that it finds the one with the seconds lowest value of
priority and excutes it and so on, until we have finished all the processes. This scheduling policy allows the user to mark some processes which we wants
to be completed first in a simple but fast way. Here, Lower the value of priority of process meaning higher the priority of process. Default priority given to each
process at start is 10. We can assign priority to particular process using setPriority function. We can assign priority value of 1 (highest priority) to the
process which we want to finish first.


To run **PRIORITY Scheduler**, use following command when compiling xv6:

``` 
$ make qemu-nox SCHEDPOLICY=PRIORITY
```

---

### Multilevel Priority Scheduler


The multilevel Priority scheduling represents a preemptive policy that includes a three priority queues (with priority value 1, 2, 3). Default value of priority
of process is 2, meaning it will be in 2nd priority queue. In this scheduling policy the scheduler will select a process from a lower queue only if no process
is ready to run at a higher queue. The algorithm first runs all the process with highest priority ie from lower valued queue and then, when they finish, it 
will consider all the process with a lower priority. This algorithm is very similar to PRIORITY, but in this case we have only three queues (low, medium, high)
and the user must select foreach process which queue the process belongs to. Priority of each process is in between 1 to 3. we can use setPriority to function 
to assign priority to particular process. The process with priority value 1 will finish first.

To run **MULTILEVEL PRIORITY Scheduler**, use following command when compiling xv6:

``` 
$ make qemu-nox SCHEDPOLICY=MULTILEVEL
```

Future Scope -- 
    we can run lottery scheuler or priority scheduler algorithm in each priority queue of multilevel (not done, here)
    
---

### Round Robin & First Come First Serve Scheduler Algorithms
    
To run ROUND ROBIN Scheduler, use following command when compiling xv6:

``` 
$ make qemu-nox SCHEDPOLICY=DEFAULT
```

To run FIRST COME FIRST SERVE Scheduler, use following command when compiling xv6:

``` 
$ make qemu-nox SCHEDPOLICY=FCFS
```

---

## Note --
Run the xv6, with single cpu for getting very promising results.
command for this :
```
$ make qemu-nox SCHEDPOLICY=["specify algo name"] CPUS=1
```








