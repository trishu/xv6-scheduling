#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "processInfo.h"

/*parent create n childreen of different types of proc
*/
int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	 struct processInfo info;
	int pids[n];
	int rets[n];
	int ticket[n];
	int j, k, p, i;
	int pid;
	int sums[3][3];
	int retime, rutime, stime;
	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			sums[i][j] = 0;
		}
	}

	#ifdef LOTTERY
		settickets(getpid(), 10);
	#else
	#ifdef PRIORITY
		setpriority(getpid(), 1);
	#else
	#ifdef MULTILEVEL
		setpriority(getpid(), 1);
	#endif
	#endif
	#endif
	for(int i = 0; i < n; i++){
		//set tickets
		j = i % 3;
		int tickets;
		int priority;
		int id = fork();
		if(id == 0){ //child
			j = (getpid() - 4) % 3;
			switch(j) {
				case 0: //CPU‐bound process (CPU):
					#ifdef LOTTERY
						tickets = 10 + i * 10;
						settickets(getpid(), tickets);
					#else
					#ifdef PRIORITY
						priority = 5 + 2*(i + 1);
						setpriority(getpid(), priority);
					#else
					#ifdef MULTILEVEL
						priority = i % 3 + 1;
						setpriority(getpid(), priority);
					#endif
					#endif
					#endif
					for(double z = 0; z < 10000.0; z+= 0.1){
				         double x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
					}
					break;
				case 1: //short tasks based CPU‐bound process (S‐CPU):	
					#ifdef LOTTERY		
					    tickets = 30 + 10*(i + 1);
						settickets(getpid(), tickets);
					#else
					#ifdef PRIORITY
						priority = 5 + 2*(i + 1);
						setpriority(getpid(), priority);
					#else
					#ifdef MULTILEVEL
						priority = i % 3 + 1;
						setpriority(getpid(), priority);					
					#endif
					#endif
					#endif
					for (k = 0; k < 100; k++){
						for (j = 0; j < 1000000; j++){
						;
						}
						yield();
					}
					break;
				case 2:// simulate I/O bound process (IO)
					#ifdef LOTTERY
						tickets = 30 + 10*(i + 1);
						settickets(getpid(), tickets);
					#else
					#ifdef PRIORITY
						priority = 5 + 2*(i + 1);
						setpriority(getpid(), priority);
					#else
					#ifdef MULTILEVEL
						priority = i % 3 + 1;
						setpriority(getpid(), priority);					
					#endif
					#endif
					#endif
					for(k = 0; k < 100; k++){
						for(double z = 0; z < 10000.0; z+= 0.1){
				         double x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
						}
						sleep(1);
					}
					break;
			}
	if(getprocinfo(getpid(), &info) == 0)
		  printf(1, "pid=%d\tppid=%d\t tickets=%d\tprio=%d\tstate=%s\tpname=%s\tctime=%d\tcntsw=%d\n", info.pid, info.parent, info.tickets, info.priority, info.state, info.pname, info.ctime, info.numberContexSwitches);
			exit();
		}
		else if(id > 0){ //parent
			pids[i] = id;
			p = (id - 4)%3;
			#ifdef LOTTERY
				if(p == 0)
					ticket[i] = 10 + i *10;
					//ticket[i] = 5;
				else if(p == 1)
					ticket[i] = 30 + 10*(i + 1);
					//ticket[i] = 10;
				else 
					ticket[i] = 30 + 10*(i + 1);
					//ticket[i] = 15;
			#else
			#ifdef PRIORITY
				if(p == 0)
					//ticket[i] = 100 + i *100;
					ticket[i] = 5 + 2*(i + 1);
				else if(p == 1)
					//ticket[i] = 50;
					ticket[i] = 5 + 2*(i + 1);
				else 
					//ticket[i] = 20;
					ticket[i] = 5 + 2*(i + 1);
			#else
			#ifdef MULTILEVEL
				if(p == 0)
					//ticket[i] = 100 + i *100;
					ticket[i] = i % 3 + 1;
				else if(p == 1)
					//ticket[i] = 50;
					ticket[i] = i % 3 + 1;
				else 
					//ticket[i] = 20;
					ticket[i] = i % 3 + 1;
			#endif
			#endif
			#endif			
		}
		else{
			printf(1, "fork failed\n");
			exit();
		}
		continue;
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 
		pid =  wait2(&retime, &rutime, &stime);
		rets[i] = pid;
		int r  = (pid - 4) % 3;
		switch(r) {
			case 0: // CPU bound processes
				printf(1, "CPU-bound, pid: %d, ready: %d, running: %d, sleeping: %d, turnaround: %d\n", pid, retime, rutime, stime, retime + rutime + stime);
				sums[0][0] += retime;
				sums[0][1] += rutime;
				sums[0][2] += stime;
					break;
			case 1: // CPU bound processes, short tasks
				printf(1, "CPU-S bound, pid: %d, ready: %d, running: %d, sleeping: %d, turnaround: %d\n", pid, retime, rutime, stime, retime + rutime + stime);
				sums[1][0] += retime;
				sums[1][1] += rutime;
				sums[1][2] += stime;
				break;
			case 2: // simulating I/O bound processes
				printf(1, "I/O bound, pid: %d, ready: %d, running: %d, sleeping: %d, turnaround: %d\n", pid, retime, rutime, stime, retime + rutime + stime);
				sums[2][0] += retime;
				sums[2][1] += rutime;
				sums[2][2] += stime;
				break;
		}

	}
	
	printf(1, "\n\n\nall children terminated\n");
	for(int i = 0; i < n; i++){
		#ifdef LOTTERY
			printf(1, "child %d, pid %d, tickets %d\n", i, pids[i], ticket[i]);
		#else
		#ifdef PRIORITY
			printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
		#else
		#ifdef MULTILEVEL
			printf(1, "child %d, pid %d, priority %d\n", i, pids[i], ticket[i]);
		#endif
		#endif
		#endif
	}
		
	printf(1, "sequence in which they exited\n");
	for(int i = 0; i < n; i++)
		printf(1, "pid %d\n", rets[i]);
	
	
	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++)
			sums[i][j] /= n;
	printf(1, "\n\nCPU bound:\nAverage ready time: %d\nAverage running time: %d\nAverage sleeping time: %d\nAverage turnaround time: %d\n\n\n", sums[0][0], sums[0][1], sums[0][2], sums[0][0] + sums[0][1] + sums[0][2]);
	printf(1, "CPU-S bound:\nAverage ready time: %d\nAverage running time: %d\nAverage sleeping time: %d\nAverage turnaround time: %d\n\n\n", sums[1][0], sums[1][1], sums[1][2], sums[1][0] + sums[1][1] + sums[1][2]);
	printf(1, "I/O bound:\nAverage ready time: %d\nAverage running time: %d\nAverage sleeping time: %d\nAverage turnaround time: %d\n\n\n", sums[2][0], sums[2][1], sums[2][2], sums[2][0] + sums[2][1] + sums[2][2]);
	exit();
}
