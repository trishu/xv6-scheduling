#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

/* This program should create n children which in turn create m children 
*/
/*multilevel .. with large no of proc ... does not work very well*/
void delay(int cnt){
	int k;
	double x = 0, z, d = 1.0;
	for(k = 0; k < 100; k++){
		for (z = 0; z < 8000.0; z+= d){
			x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
		}
		sleep(1);
	}
}

void delay1(int cnt){
	int k;
	double x = 0, z, d = 1.0;
	for(k = 0; k < 100; k++){
		for (z = 0; z < 800000.0; z+= d){
			x =  x + 3.14 * 89.64;   // useless calculations to consume CPU time
		}
		sleep(1);
	}
}


int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	int pids[n+n*m];
	int rets[n+n*m];
	int ticket[n+n*m];
	int pk = 0, rk = 0, tk = 0;

	int curpid = getpid();
	#ifdef PRIORITY
		setpriority(curpid, 1);
	#else
	#ifdef MULTILEVEL
		setpriority(getpid(), 1);
	#endif
	#endif
	
	for(int i = 0; i < n; i++){
		//set tickets
		int tickets = 15;
		int priority  = 1;
		int id = fork();
		if(id == 0){ //child
			int ppid = getpid();
			#ifdef PRIORITY
				printf(1, "pid %d, ppid %d, priority %d\n", ppid, curpid, priority);
				setpriority(ppid, priority);
			#else
			#ifdef MULTILEVEL
				printf(1, "pid %d, ppid %d, priority %d\n", ppid, curpid, priority);
				setpriority(ppid, priority);
			#else
			#ifdef LOTTERY
				printf(1, "pid %d, ppid %d, tickets %d\n", ppid, curpid, tickets);
				settickets(ppid, tickets);
			#endif
			#endif
			#endif
			

			for(int j = 0; j < m; j++){


				int pid = fork();
				if(pid == 0){
					#ifdef LOTTERY
						int t = 10*j +  5*i + 20;
						printf(1, "pid %d, ppid %d, tickets %d\n", getpid(), ppid,  t);
						settickets(getpid(), t);
						sleep(5);
						delay1(20);
					#else
					#ifdef PRIORITY
						int p = 10 + 2*(j + 1) + 5*(i + 1) + 10*i;
						printf(1, "pid %d, ppid %d, priority %d\n", getpid(), ppid,  p);
						setpriority(getpid(), p);
						sleep(5);
						delay(20);
					#else
					#ifdef MULTILEVEL
						int p = j % 3 + 1;
						printf(1, "pid %d, ppid %d, priority %d\n", getpid(), ppid,  p);
						setpriority(getpid(), p);
						delay(20);
					#endif
					#endif
					#endif
					exit();
				}
			}
			for(int j = 0; j < m; j++){
					int ret = wait();
					printf(1, "wait is over for child of child %d\n", ret);
			}
			exit();
		}
	}
	
	//parent wait for all his childrens to terminate
	for(int i = 0; i < n; i++){ 

		int ret = wait(); 
		printf(1, "wait is over for child %d\n", ret);
	}
		

	
	exit();
}
